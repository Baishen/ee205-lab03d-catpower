###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 03d - Cat Power - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Build a Cat Power C++ Program
###
### @author  Baishen Wang<baishen@hawaii.edu>
### @date    30_01_ 2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = g++
CFLAGS = -g -Wall

TARGET = catPower

all: $(TARGET)

hello: catPower.cpp
		$(CC) $(CFLAGS) -o $(TARGET) catPower.cpp

test: catPower
		./catPower

clean:
		rm -f $(TARGET) *.o

